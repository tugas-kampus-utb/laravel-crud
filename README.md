# Laravel CRUD
## Installation
Use CMD or Terminal and clone this repo:

1. Clone or Download This Repo:
```bash
$ git clone git@gitlab.com:tugas-kampus-utb/laravel-crud.git
```

2. After Cloning this Repo, you must to install package depencies use composer on terminal

```composer_install
$ composer install
```

3. Create Your Database on mysql
4. Create your Environment file, copy .env.example and edit the parts listed below, after that save as .env
```env
...

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306 for mysql or 5432 for pgsql
DB_DATABASE=your_database
DB_USERNAME=your_database_username
DB_PASSWORD=your_database_password

...
```

5. Generate Laravel Key
```bash
$ php artisan key:generate
```

6. Migrate Database Tables:
```bash
$ php artisan migrate
```

7. View Routing Web:
```bash
$ php artisan route:list
```

8. Run project:
```bash
$ php artisan serve
```
Note: In case you are running this demo on your local machine, use http://localhost:8000/mahasiswa to access the application from your browser.