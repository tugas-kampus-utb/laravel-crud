<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;
use App\Repositories\Crud\CrudRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Redirect;

class MahasiswaController extends Controller
{
    protected $crudRepository;
    public function __construct(Mahasiswa $mahasiswa)
    {
        $this->crudRepository = new CrudRepository($mahasiswa);
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $mahasiswa = $this->crudRepository->read($request);
        return view('mahasiswa.index', [ 'mahasiswa' => $mahasiswa['contents'] ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        return view('mahasiswa.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $payload = $request->all();
        $request->validate([
            'nama' => 'required',
            'kelas' => 'required',
        ], $payload);
        $res = $this->crudRepository->create($payload);
        if (JsonResponse::HTTP_CREATED != $res['code']) {
            $request->session()->flash('errors', $res['message']);    
            return redirect('/mahasiswa/create');
        }
        $request->session()->flash('success', $res['message']);
        return redirect('/mahasiswa');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $mahasiswa = $this->crudRepository->readById($id);
        return view('mahasiswa.show', [ 'mahasiswa' => $mahasiswa['contents'] ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $mahasiswa = $this->crudRepository->readById($id);
        return view('mahasiswa.edit', [ 'mahasiswa' => $mahasiswa['contents'] ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $payload = $request->all();
        $request->validate([
            'nama' => 'required',
            'kelas' => 'required',
        ], $payload);
        $payload = array(
            "nama" => $request->input('nama'),
            "kelas" => $request->input('kelas'),
        );
        $res = $this->crudRepository->update($payload, $id);
        if (JsonResponse::HTTP_OK != $res['code']) {   
            return Redirect::back()->withErrors(['message' => $res['message']]);
        }
        $request->session()->flash('success', $res['message']);
        return redirect('/mahasiswa');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $res = $this->crudRepository->delete($id);
        session()->flash('success', $res['message']);
        return redirect('/mahasiswa');
    }
}
